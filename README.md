# socket io + django

## setup

- `cd django_server && python manage.py migrate`

- `cd ../socket_server && npm install`

## To get up and running

- ` cd django_server && python manage.py runsever`

- open new terminal and `cd socket_server`

- `node app.js`
