/*
 * js code for index.html
 */

var socket = io("http://localhost:3000");

var personName = '';

/*
 *  sets up ajax so post requests have a csrf_token
 */
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});


/*
 * Get the person who visits the page's name
 */

$(document).ready(function personVisited() {
    while (personName == '' || personName == null) {
        personName = prompt("Please enter your name", "Name");
    }

    $.post("userjoined/", 
        {
            name: personName,
        },
        function(data, status) {
        }
    );
});



/*
 * responds to a user joining the chat
 */
socket.on('join', function(data) {
    console.log(data.name + " has joined");

    var html = "<h5>" + data.name + " has joined </h5>";
    
    $("#notifications").append(html);

});

/*
 * Responds to receiving a message
 */
socket.on('getmessage', function(data) {
    console.log(data.message);

    var html = "<h3>" + data.name + ": " + data.message + "</h3>";
    
    $("#notifications").append(html);

});

/*
 * sends a message
 */ 
$("#msg-input").on('keypress', function(event) {
    if (event.which === 13) {
        console.log(personName);
        $.post("sentmessage/", 
            {
                message: $(this).val(),
                name: personName,
            },
            function(data, status) {
                $('#msg-input').val('');
            }
        );
    }
});




