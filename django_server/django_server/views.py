from django.shortcuts import render
from django.http import JsonResponse, Http404
import urllib2
import urllib


'''
    serves the index page
'''
def home(request):
    return render(request, 'index.html')

'''
    tells node server that a user has joined

    params: name
'''
def userjoined(request):
    try:
        if request.method == "POST":
            name = request.POST['name']

            url = 'http://localhost:3000/userjoined'
            p_data = [('name', name)]
            result = urllib2.urlopen(url, urllib.urlencode(p_data))

            if result:
                return JsonResponse({'success': True})

            return JsonResponse({'success': False})

        else:
            raise Http404("use post")
            
    except Exception as e:
        print e
        raise Http404(e)


'''
    sends a message to node server

    params: name, message
'''
def sentmessage(request):
    try:
        if request.method == "POST":
            message = request.POST['message']
            name = request.POST['name']

            url = 'http://localhost:3000/sentmessage'
            p_data = [('message', message), 
                      ('name', name)]
            result = urllib2.urlopen(url, urllib.urlencode(p_data))
        
            if result:
                return JsonResponse({'success': True})

            return JsonResponse({'success': False})
        else:
            raise Http404("use post")
    except Exception as e:
        print e
        raise Http404(e)
        
