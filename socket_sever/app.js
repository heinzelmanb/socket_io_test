var app = require('express')();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());

var server = require('http').Server(app);
var io = require('socket.io')(server);

app.get('/', function(req, res) {
    res.send('<h1> socket_server </h1>');
});

app.post('/userjoined', function(req, res) {
    console.log("A user has joined...");
    var name = req.body.name;
    io.emit('join', {name: name});
    res.send(true);
});

app.post('/sentmessage', function(req, res) {
    console.log("received message...");
    var message = req.body.message;
    var from = req.body.name;
    io.emit('getmessage', {message: message, name: from});
    console.log(from);
    res.send(true);
});

server.listen(3000, function() {
  console.log("Spinning up server");      
});
